﻿using System;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Welcome to Employee Management System......\n\n");
        List<Employee.employee> emp = new List<Employee.employee>();
        bool x = true;
        do  // to run the menu atleast once
        {
            Console.WriteLine("1. Add New Employee \n" +
                              "2. Update Employee Details \n" +
                              "3. Delete Employee \n" +
                              "4. View Employee Details \n" +
                              "5. Exit ");

            Console.WriteLine("\nPlease select an option: ");

            int a = int.Parse(Console.ReadLine());
            int len = emp.Count;

            switch (a)
            {
                case 1:

                    Console.WriteLine("Please Enter EmployeeID to Add\n");
                    string empid = Console.ReadLine();

                    if (len != 0)
                    {
                        if (Employee.Choice.findID(empid, emp))
                        {
                            Console.WriteLine("Employee already exists.\n");
                            break;
                        }
                    }

                    emp.Add(Employee.Choice.Add(empid));

                    break;

                case 2:

                    if (len != 0)
                    {
                        Console.WriteLine("Please Enter EmployeeID to Update:");
                        Employee.Choice.Update(Console.ReadLine(), emp);
                    }
                    else
                    {
                        Console.WriteLine("Add Employees.\n");
                    }

                    break;

                case 3:

                    if (len != 0)
                    {
                        Console.WriteLine("Please Enter EmployeeID to Delete");
                        Employee.Choice.Delete(Console.ReadLine(), emp);
                    }
                    else
                    {
                        Console.WriteLine("Add Employees.\n");
                    }

                    break;

                case 4:

                    if (len != 0)
                    {
                        Console.WriteLine("1. For a specific employee\n2. For all the employees\n");
                        Employee.Choice.View(Convert.ToInt32(Console.ReadLine()), emp);
                    }
                    else
                    {
                        Console.WriteLine("Add Employees.\n");
                    }

                    break;

                case 5:

                    Console.WriteLine("Thank you.");
                    x = false;

                    break;

                default:

                    Console.WriteLine("Please Select From The Options Mentioned\n");

                    break;

            }
        } while (x);
    }
}

namespace Employee
{
    class employee
    {
        private string empId;
        private string empName;
        private string empDob;
        private string[] empQual;
        private float empSal;
        private string empRole;
        private int empAge;

        public string getId()
        {
            return empId;
        }
        public string getName()
        {
            return empName;
        }
        public int getAge()
        {
            return empAge;
        }
        public string getDob()
        {
            return empDob;
        }
        public string[] getQual()
        {
            return empQual;
        }
        public float getSal()
        {
            return empSal;
        }
        public string getRole()
        {
            return empRole;
        }

        public void setId(string id)
        {
            this.empId = id;
        }
        public void setName(string name)
        {
            this.empName = name;
        }
        public void setDob(string dob)
        {
            this.empDob = dob;
            this.empAge = Age(dob);
        }
        public void setQual(string[] qual)
        {
            this.empQual = qual;
        }
        public void setSal(float sal)
        {
            this.empSal = sal;
        }
        public void setRole(string role)
        {
            this.empRole = role;
        }
        private int Age(string empob)
        {
            int year = Convert.ToInt32(empob.Substring(6, 4));
            int empAge = 2019 - year;
            return empAge;
        }
    }

    class Choice
    {
        static int pos; //index of the emp found
        public static employee Add(string empid)
        {
            string name;
            string dob;
            string[] qual = new string[10];
            float sal;
            string role;
            int no;

            employee emp = new employee();
            emp.setId(empid);

            Console.WriteLine("Enter Employee name");
            name = Console.ReadLine();
            emp.setName(name);

            Console.WriteLine("Enter Employee Date of Birth (dd/mm/yyyy or dd-mm-yyyy)");
            dob = Console.ReadLine();
            bool t = true;
            while (t)
            {
                if (dob.Length == 10 && 1919 <= Convert.ToInt32(dob.Substring(6, 4)) && Convert.ToInt32(dob.Substring(6, 4)) < 2001) //date length and age min 18 and max 100 required
                {
                    emp.setDob(dob);
                    t = false;
                }
                else
                {
                    Console.WriteLine("Enter valid Employee Date of Birth (dd/mm/yyyy or dd-mm-yyyy)");
                    dob = Console.ReadLine();
                }
            }

            Console.WriteLine("Enter Employee Number of Qualifications");  //to get arraysize for qualifications
            no = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter the Qualifications now:");

            for (int i = 0; i < no; i++)
            {
                qual[i] = Console.ReadLine();
            }
            emp.setQual(qual);

            Console.WriteLine("Enter Employee Salary");
            sal = Convert.ToSingle(Console.ReadLine());
            emp.setSal(sal);

            Console.WriteLine("Enter Employee Role");
            role = Console.ReadLine();
            emp.setRole(role);

            Console.WriteLine("Emp added\n");
            return emp;
        }
        public static void Update(string empid, List<employee> emp)
        {
            if (findID(empid, emp)) 
            {
                Console.WriteLine("1. Update Salary\n2. Update Role ");
                switch (Convert.ToInt32(Console.ReadLine()))
                {
                    case 1:
                        Console.WriteLine("Enter the new Salary");
                        emp[pos].setSal(Convert.ToInt32(Console.ReadLine()));
                        Console.WriteLine("Employee Salary Updated\n");
                        break;

                    case 2:
                        Console.WriteLine("Enter the new Role");
                        emp[pos].setRole(Console.ReadLine());
                        Console.WriteLine("Employee Role Updated\n");
                        break;

                    default:
                        Console.WriteLine("Invalid input");
                        Console.WriteLine("Emp not updated\n");
                        break;
                }

            }
            else
            {
                Console.WriteLine("Employee not found\n");
            }

        }
        public static void Delete(string empid, List<employee> emp)
        {
            if (findID(empid, emp))
            {
                emp.RemoveAt(pos);
                Console.WriteLine("Emp Deleted\n");
            }
            else
            {
                Console.WriteLine("Employee not found.\n");
            }

        }
        public static void View(int c, List<employee> emp)
        {
            switch (c)
            {
                case 1:
                    Console.WriteLine("Please Enter EmployeeID to View\n");
                    if (findID(Console.ReadLine(), emp))
                    {
                        Console.WriteLine("Empid = {0} \n" +
                            "Name = {1} \n" +
                            "Dob = {2} \n" +
                            "Age = {3} \n" +
                            "Salary = {4} \n" +
                            "Role = {5} \n",
                            emp[pos].getId(),
                            emp[pos].getName(),
                            emp[pos].getDob(),
                            emp[pos].getAge(),
                            emp[pos].getSal(),
                            emp[pos].getRole()
                            );
                        Console.WriteLine("Qualifications...");
                        string[] qual = emp[pos].getQual();
                        for (int i = 0; i < (qual).Length; i++)
                        {
                            Console.WriteLine(qual[i]);
                        }
                    }
                    break;

                case 2:
                    for (int i = 0; i < emp.Count; i++)
                    {
                        Console.WriteLine(" Empid = {0} \n" +
                            "Name = {1} \n" +
                            "Dob = {2} \n" +
                            "Age = {3} \n" +
                            "Salary = {4} \n" +
                            "Empid = {5} \n",
                            emp[i].getId(),
                            emp[i].getName(),
                            emp[i].getDob(),
                            emp[i].getAge(),
                            emp[i].getSal(),
                            emp[i].getRole()
                            );
                        Console.WriteLine("Qualifications...");
                        string[] qual = emp[i].getQual();
                        for (int j = 0; j < (qual).Length; j++)
                        {
                            Console.WriteLine(qual[j]);
                        }
                    }

                    break;
            }

            Console.WriteLine("Emp shown\n");
        }
        public static Boolean findID(string empId, List<employee> emp) // find the employee and note the index
        {
            int i;

            for (i = 0; i < emp.Count; i++)
            {
                if (empId.Equals(emp[i].getId()))
                {
                    pos = i;
                    return true;
                }
            }

            return false;

        }
    }
}

